﻿using UnityEngine;
using UnityEngine.Networking;

public class SpawnThings : NetworkBehaviour {

	public GameObject Fotelprefab;
	//public GameObject Chairprefab;

	//public GameObject Cube;
	//public GameObject Slup;


	//public Transform spawnPoint1;
	//public Transform spawnPoint3;
	//public Transform spawnPoint4;

	// Use this for initialization
	void Start () {
		CmdSpawn();
	}
	
	// Update is called once per frame
	void Update () {
		

	}

	[Command]
	void CmdSpawn()
	{
		var fotel = (GameObject)Instantiate (Fotelprefab, new Vector3(-3.5f, 2.0f, 1.85f), Quaternion.Euler(new Vector3(0f,32f,0f)));
		//var cube = (GameObject)Instantiate (Cube, spawnPoint4.position, spawnPoint4.rotation);
		//var slup = (GameObject)Instantiate (Slup, spawnPoint4.position, spawnPoint4.rotation);

		//slup.GetComponent<follow> ().catch_me = cube;

		//NetworkServer.SpawnWithClientAuthority(cube, connectionToClient);
		//NetworkServer.SpawnWithClientAuthority(slup, connectionToClient);
		NetworkServer.SpawnWithClientAuthority(fotel, connectionToClient);
	}
}
