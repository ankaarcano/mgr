﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class CurrentCamera : NetworkBehaviour {

	public Camera currentCamera;
	private Camera followCamera;
	private Camera independentCamera;
	private int liczbaZmian = 0;
	public GameObject temp;
	// czy niezależna kamera jest włączona?
	bool iCam; 

	// Use this for initialization
	void Start () {

		GameObject temp = GameObject.FindGameObjectWithTag ("cam");
		followCamera = temp.GetComponent<Camera>();

		GameObject temp2 = GameObject.FindGameObjectWithTag ("iCam");
		independentCamera = temp2.GetComponent<Camera> ();
		temp2.GetComponent<AudioListener> ().enabled=false;
		independentCamera.enabled = false;
		iCam = false;

		if (isServer) {
			currentCamera = Camera.main;
		} 
		else 
		{
			currentCamera = followCamera;
		}
	}
	//*********************** koniec Startu ***********************

	// Update is called once per frame
	void Update () {
		if (isServer) {
			currentCamera = Camera.main;
		} 
		else
		{
			if (Input.GetKeyDown (KeyCode.C)) 
			{
				liczbaZmian++;
				SwitchCamera ();
			}
		}
		// rozpoznawanie na jakie obiekty patrzę
		//CmdLookingAt();
		//CmdOnMouseDrag ();

	}
	//*********************** koniec Update'a ***********************



	public void SwitchCamera()
	{

		if(!iCam ) 
		{
			independentCamera.enabled = true;
			followCamera.enabled = false;
			currentCamera = independentCamera;
			//Debug.Log (liczbaZmian +" zmiana: "+currentCamera.name);
		} 
		if(iCam)
		{
			followCamera.enabled = true;
			independentCamera.enabled = false;
			currentCamera = followCamera;
			//Debug.Log (liczbaZmian +" zmiana: "+currentCamera.name);
		}
		iCam = !iCam;
	}


	/*public GameObject LookAt()
	{
		RaycastHit hit;
		if(Physics.Raycast(currentCamera.ScreenPointToRay(Input.mousePosition),out hit, 25.0f,LayerMask.GetMask("Client_interaction")))
			{
			//Debug.Log (hit.collider.tag);
			temp = GameObject.FindGameObjectWithTag (hit.collider.tag);
				//if(Input.GetMouseButtonDown(0))
				{
				
				}

			}

		return temp;
	}


	[Command]
	public void CmdOnMouseDrag()
	{
		if (Input.GetMouseButtonDown (0)) {
			Vector3 mousePosition = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 2.0f);

			//Vector3 objectPosition = currentCamera.ScreenToWorldPoint (mousePosition);
			Vector3 objectPosition = currentCamera.ScreenToViewportPoint (mousePosition);
			Debug.Log (objectPosition);
			temp.transform.position = objectPosition;
		}
		//Debug.Log (go + " pozycja x: " + hit.point.x);
		//Debug.Log (go + " pozycja y: " + hit.point.y);
	}

	[Command]
	public void CmdLookingAt ()
	{
		RaycastHit hit;
		if (Physics.Raycast (currentCamera.ScreenPointToRay (Input.mousePosition), out hit, 25.0f, LayerMask.GetMask ("Client_interaction"))) {
			Debug.Log (hit.collider.tag);
			temp = GameObject.FindGameObjectWithTag (hit.collider.tag);
			Debug.Log ("Nazwa obiektu paczonego: " + temp.name);
		}
	}*/
}

